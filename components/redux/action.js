import { COIN_ACTIONS } from "./constants";

export const addCoin = (coin) => {
  return {
    type : COIN_ACTIONS.ADD_COIN,
    payload : coin
  }
}

export const updateCoin = (coin) => {
  return {
    type : COIN_ACTIONS.UPDATE_COIN,
    payload : coin
  }
}

export const deleteCoin = () => {
  return {
    type : COIN_ACTIONS.ADD_COIN,
  }
}