import { COIN_ACTIONS } from "./constants";
const initialState = {
    coin : []
};


export const coinReducer = (state = initialState, {type, payload}) => {
    switch(type){
        case COIN_ACTIONS.ADD_COIN : 
            return {...state, ...payload}
        default:
            return state
        
    }
}