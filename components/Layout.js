import Head from 'next/head';
import Link from 'next/link';

const Layout = ({ children, title = 'Crypto Tracker' }) => {
  return (
    <div className='layout'>
      <Head>
  
        <title>{title}</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <header>
        <h1 style={{marginTop:"40px",textAlign:"center"}}>Click to table rows</h1>
      </header>
      <main>{children}</main>

    </div>
  );
};

export default Layout;
